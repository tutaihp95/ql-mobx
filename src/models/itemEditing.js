import { types } from "mobx-state-tree"

const itemEditing = types
  .model({
    name: types.string,
    status: false,
    id: types.identifierNumber
  })

export default itemEditing
