import { types, destroy } from "mobx-state-tree"
import { SHOW_ALL, SHOW_HIDE, SHOW_ACTIVE } from "../constants/TaskFilters"
import TaskItem from './TaskItem'
import ItemEditing from './itemEditing'

const filterType = types.union(...[SHOW_ALL, SHOW_HIDE, SHOW_ACTIVE].map(types.literal))
const TASK_FILTERS = {
  [SHOW_ALL]: () => true,
  [SHOW_ACTIVE]: task => task.status,
  [SHOW_HIDE]: task => !task.status,
}
const TaskStore = types
  .model({
    tasks: types.array(TaskItem),
    filter: types.optional(filterType, SHOW_ALL),
    filterName: types.union(types.string, types.null, types.undefined),
    isDisplayForm: false,
    ItemEditings: types.array(ItemEditing),
    editItemId: types.union(types.number, types.null, types.undefined),
    editItem: types.union(types.number, types.null, types.undefined),
  })
  .views(self => ({
    get completedCount() {
      return self.tasks.reduce((count, task) => (task.completed ? count + 1 : count), 0)
    },
    get activeCount() {
      return self.tasks.length - self.completedCount
    },
    get filteredtasks() {
      let filteredListName = self.tasks.filter(task =>
        task.name.toLowerCase().includes(self.filterName)
      );
      let filteredListStatus = self.tasks.filter(TASK_FILTERS[self.filter])

      if(self.filterName){
          return filteredListName;
      }
      if(filteredListStatus){
        return filteredListStatus
      }
      return self.tasks
    },

    get getItemEdit() {
      let editTask = self.tasks.find(task =>
        task.id === self.editItemId,
      )
      return editTask
    }
  }))
  .actions(self => ({
    // actions
    saveTask(name, status) {
      const idRandom = self.tasks.reduce((maxId, task) => Math.max(task.id, maxId), -1) + 1;
      if (self.editItem || self.editItem===0) {
        // var IDTasks = self.tasks.map((task) => task.id);
        // var result = -1
        // self.tasks[index] = task;

        // IDTasks.forEach((IDTask)=>{
        //   if(IDTask === self.editItemId ){
        //     result= IDTask; 
        //     console.log(result);
        //   }
        // });
        var index = self.tasks.findIndex((task) => task.id === self.editItem);
        var task = {
          id: self.tasks[index].id,
          name: name,
          status: status
        };
        self.tasks[index] = task
      }
      else ( 
        self.tasks.unshift({
          id: idRandom,
          name,
          status
        })
      )
    },
    addTask(name, status) {
      const id = self.tasks.reduce((maxId, task) => Math.max(task.id, maxId), -1) + 1
      self.tasks.unshift({
        id,
        name,
        status
      })
      self.ItemEditings.splice(0, 1);
    },
    removeTask(task) {
      destroy(task)
    },
    completeAll() {
      const areAllMarked = self.tasks.every(task => task.completed)
      self.tasks.forEach(task => (task.completed = !areAllMarked))
    },
    clearCompleted() {
      self.tasks.replace(self.tasks.filter(task => task.completed === false))
    },
    setFilter(filter) {
      self.filter = filter
    },
    setFilterName(filterName) {
      self.filterName = filterName
    },
    toggleForm() {
      if (self.editItem) {
        self.isDisplayForm = true
      } else (
        self.isDisplayForm = !self.isDisplayForm
      )
      self.editItem = self.editItemId = null
    },
    openForm() {
      self.isDisplayForm = true
    },
    closeForm() {
      self.isDisplayForm = false
      self.ItemEditings.splice(0, 1);
      self.editItem = self.editItemId = null
    },
    onResetForm() {
      self.isDisplayForm = false
    },
    onEditTask(id, name, status) {
      self.isDisplayForm = true;
      self.editItem = self.editItemId = id;
    },

    removeEditTask() {
      self.editItemId = null
    }
  }))

export default TaskStore
