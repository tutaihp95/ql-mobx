import { types, getRoot } from "mobx-state-tree"

const TaskItem = types
  .model({
    id: types.identifierNumber,
    name: types.string,
    status: false,
    // isEdit: false,
  })
  .actions(self => ({
    remove() {
      getRoot(self).removeTask(self)
    },
    edit() {
      // name = self.name;
      // status = self.status;
      // console.log(self.name);
    },
    updateStatus() {
      self.status = !self.status
    }
  }))
  .views(self => ({
    get statusShow() {
      return self.status === true ? 'Kích hoạt' : 'Ẩn'
    }
  }))

export default TaskItem
