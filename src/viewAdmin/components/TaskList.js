import React, { Component } from 'react'
import TaskItem from './TaskItem'
import classnames from "classnames"
import { SHOW_ALL, SHOW_HIDE, SHOW_ACTIVE } from "../../constants/TaskFilters"
import { observer } from "mobx-react"

const FILTER_TITLES = {
  [SHOW_ALL]: "Tất cả",
  [SHOW_ACTIVE]: "Kích hoạt",
  [SHOW_HIDE]: "Ẩn"
}

class TaskList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filterName: ''
    };
  }
  // EditFormList = (id) => {
  //   this.props.EditFormApp(id);
  // }

  // onChange = (event) => {
  //   var target = event.target;
  //   var name = target.name;
  //   var value = target.value;
  //   this.props.onFilterApp(
  //     name === 'filterName' ? value : this.state.filterName,
  //     name === 'filterStatus' ? value : this.state.filterStatus,
  //   );
  //   this.setState({
  //     [name]: value
  //   });
  // }
  onChange=(e)=>{
    var target = e.target;
    var value = target.value.toString().toLowerCase();
    this.setState({
      filterName: value
    });
    this.props.store.setFilterName(value);
  }

  get filtered() {
    let filteredList = this.props.store.tasks.filter(task =>
      task.name.toLowerCase().includes(this.state.filterName)
    );

    if (filteredList.length) return filteredList;
    return this.props.store.tasks;
  }
  renderFilterName(){
    return (
        <input
          type="text"
          className="form-control"
          name="filterName"
          value={this.state.filterName}
          onChange={ this.onChange }
        />
    )
  }

  renderFilterStatus(filter) {
    const title = FILTER_TITLES[filter]
    const { store } = this.props
    const selectedFilter = store.filter

    return (
      // eslint-disable-next-line
      <a 
        className={classnames({ selected: filter === selectedFilter })}
        style={{ cursor: "pointer" }}
        onClick={() => store.setFilter(filter)}
      > 
        {title} 
      </a>
    )
  }

  render() {
    // var { filterName, filterStatus } = this.state;
    var elmTasks = this.props.store.filteredtasks.map((task, index) => {
      return <TaskItem
        key={index}
        index={index}
        task={task}
        store={this.props.store}
      />
    })
    return (
      <div className="row mt-3">
        <div className="col-xl-12 col-sm-12 col-md-12 col-lg-12">
          <table className="table table-bordered table-hover">
            <thead>
              <tr>
                <th>STT</th>
                <th>Tên</th>
                <th>Trạng thái</th>
                <th>Hành động</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td />
                <td>
                 { this.renderFilterName()}
                </td>
                <td>
                  <ul
                    name="filterStatus"
                    className="filterStatus"
                  >
                    {[SHOW_ALL, SHOW_ACTIVE, SHOW_HIDE].map(filter => (
                      <li key={filter}>
                        {this.renderFilterStatus(filter)}
                      </li>
                    ))}
                  </ul>
                </td>
                <td />
              </tr>
              {elmTasks}
            </tbody>
          </table>
        </div>
      </div>
    );
  };
}

export default observer(TaskList);
