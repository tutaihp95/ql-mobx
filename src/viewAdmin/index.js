import React, { Component } from 'react';
import TaskControl from './components/TaskControl';
import TaskList from './components/TaskList';
import TaskForm from './components/TaskForm'
import { observer } from "mobx-react"

class ViewAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDisplay: false,
      tasks: [],
      taskEdit: null,
      filterName: '',
      filterStatus: -1,
      keyword: '',
      sortBy: 'name',
      sortValue: 1
    };
  }

  onSearchApp = (keyword) => {
    this.setState({
      keyword: keyword
    })
  }

  onSort = (sortBy, sortValue) => {
    this.setState({
      sortBy: sortBy,
      sortValue: sortValue
    });
  }

  render() {
    var { store } = this.props;

    // if(keyword){
    //     tasks=tasks.filter((task) =>{
    //         return task.name.toLowerCase().indexOf(keyword) !==-1;
    //     });
    // }

    // if(sortBy === 'name'){
    //     tasks.sort((a,b) =>{
    //         if(a.name > b.name) return sortValue;
    //         else if(a.name < b.name) return -sortValue;
    //         else return 0;
    //     });
    // }else{
    //     tasks.sort((a,b) =>{
    //         if(a.status > b.status) return -sortValue;
    //         else if(a.status < b.status) return sortValue;
    //         else return 0;
    //     });
    // }
    var elmTaskForm = store.isDisplayForm ? <TaskForm store={store} /> : '';
    return (
      <div className="container">
        <a className="btn btn-success position-absolute" style={{ right: 0 }} href="/">
          <span className="button-label">View</span>
        </a>

        <div className="row">
          <div className="col-12 text-center">
            <h1> Quản lý công việc </h1>
          </div>
          <div className={store.isDisplayForm === false ? '' : 'col-xl-4 col-sm-4 col-md-4 col-lg-4'} >
            {/* form */}
            {elmTaskForm}
            {/* { store.isDisplayForm===true ? <TaskForm/>:''} */}
          </div>
          <div className={store.isDisplayForm === false ? 'col-xl-12 col-sm-12 col-md-12 col-lg-12' : 'col-xl-8 col-sm-8 col-md-8 col-lg-8'}>
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => store.toggleForm()}
            >
              <span className="fas fa-plus mr-2" /> Thêm Công Việc
            </button>

            {/* search - sort */}
            <TaskControl
            // onSearchApp={this.onSearchApp}
            // onSort = { this.onSort }
            // sortBy = {sortBy}
            // sortValue= {sortValue}
            />
            {/* list */}
            <TaskList
              store={store}
            />
          </div>
        </div>
      </div>
    );
  };
}

export default observer(ViewAdmin);
