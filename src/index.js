import React from 'react'
import { render } from 'react-dom'
import { getSnapshot, destroy, onSnapshot } from "mobx-state-tree"
import { connectReduxDevtools } from "mst-middlewares"
import TaskStore from "./models/Task"
import './index.css'
import './myStyle/style.scss';
import './view/myStyle/styleView.scss';
import App from './App'
import * as serviceWorker from './serviceWorker'

const localStorageKey = "tasks-note"

const initialState = localStorage.getItem(localStorageKey)
  ? JSON.parse(localStorage.getItem(localStorageKey))
  : {}
let store
let snapshotListener

function createTaskStore(snapshot) {
  if (snapshotListener) snapshotListener()
  if (store) destroy(store)

  store = TaskStore.create(snapshot)

  connectReduxDevtools(require("remotedev"), store)
  snapshotListener = onSnapshot(store, snapshot =>
    localStorage.setItem(localStorageKey, JSON.stringify(snapshot))
  )

  return store
}

function renderApp(App, store) {
  render(<App store={store} />, document.getElementById("root"))
}

renderApp(App, createTaskStore(initialState))

if (module.hot) {
  module.hot.accept(["./models/Task"], () => {
    renderApp(App, createTaskStore(getSnapshot(store)))
  })

  module.hot.accept(["./App"], () => {
    renderApp(App, store)
  })
}
serviceWorker.unregister();
