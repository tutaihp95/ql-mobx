import React, { Component } from 'react'
import { observer } from 'mobx-react'

class TaskForm extends Component {

  constructor(props) {
    super(props);
    this.state = ({
      id: '',
      name: this.props.name || '',
      status: this.props.status || false
    })
  }

  onChange = (event) => {
    const { store } = this.props
    if (store.getItemEdit) {
      this.setState({
        id: store.getItemEdit.id,
      })
      // remove editItem
      store.removeEditTask()
    }
    var target = event.target;
    var name = target.name;
    var value = target.value;
    if (name === 'status') {
      value = target.value === 'true' ? true : false;
    }
    this.setState({
      [name]: value,
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const name = this.state.name;
    const status = this.state.status;
    if (name.length !== 0) {
      this.props.store.saveTask(name, status);
      this.props.store.closeForm();
    }
  }

  render() {
    var { store } = this.props;

    return (
      <div className="card text-left">
        <div className="card-header">
          {store.editItem || store.editItem===0 ? 'Cập nhật công việc' : 'Thêm công việc'}
          <span className="fas fa-times-circle" onClick={() => store.closeForm()} />
        </div>
        <div className="card-body">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label>Tên :</label>
              <input
                type="text"
                className="form-control"
                name="name"
                value={(store.getItemEdit && store.getItemEdit.name) || this.state.name}
                onChange={this.onChange}
              />
            </div>
            <div className="form-group">
              <label>Trạng thái :</label>
              <select
                name="status"
                className="form-control"
                value={(store.getItemEdit && store.getItemEdit.status) || this.state.status}
                onChange={this.onChange}
              >
                <option value={true}>Kích hoạt</option>
                <option value={false}>Ẩn</option>
              </select>
            </div>
            <div className="text-center">
              <button className="btn btn-warning" type="submit">
                <span className="fas fa-plus mr-2" /> Lưu lại
            </button>
              <button
                className="btn btn-danger"
                type="button"
                onClick={this.onReset}
              >
                <span className="fas fa-times mr-2" /> Hủy bỏ
            </button>
            </div>
          </form>
        </div>
      </div>
    );
  };
}

export default observer(TaskForm);
