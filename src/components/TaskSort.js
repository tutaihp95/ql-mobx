import React, {Component} from 'react';

class TaskSort extends Component{
   
  onClick = (sortBy, sortValue) =>{
      this.props.onSort(sortBy, sortValue);
  }

  render() {
    var iconCheck = <span className="fas fa-check ml-2"></span>;
    return (
      <div className="col-xl-6 col-sm-6 col-md-6 col-lg-6">
        <div className="dropdown">
          <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sắp xếp <span />
          </button>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <div 
              className="dropdown-item" 
              onClick={ ()=> this.onClick('name',1)}
            >
              <span className="fas fa-sort-alpha-down mr-3" />Tên A-Z 
              { (this.props.sortBy==='name' && this.props.sortValue=== 1) ? iconCheck : ''}
            </div>
            <div className="dropdown-item"  onClick={ ()=> this.onClick('name',-1)}>
              <span className="fas fa-sort-alpha-down-alt mr-3" />Tên Z-A
              { (this.props.sortBy==='name' && this.props.sortValue=== -1) ? iconCheck : ''}
            </div>
            <div className="dropdown-divider" />
            <div className="dropdown-item" onClick={ ()=> this.onClick('status',1)}>Trạng thái kích hoạt
            { (this.props.sortBy==='status' && this.props.sortValue=== 1) ? iconCheck : ''}
            </div>
            <div className="dropdown-item"  onClick={ ()=> this.onClick('status',-1)}>Trạng thái ẩn
            { (this.props.sortBy==='status' && this.props.sortValue=== -1) ? iconCheck : ''}
            </div>
          </div>
        </div>
      </div>
    );
  };
}

export default TaskSort;
