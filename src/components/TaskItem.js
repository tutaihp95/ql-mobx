import React, { Component } from 'react';
import { observer } from "mobx-react"
class TaskItem extends Component {

  state = {
    id: '',
    name: '',
    status: false
  }
  
  dataEdit = (task) => {
    const id = task.id;
    const name = task.name;
    const status = task.status;
    this.props.store.onEditTask(id, name, status);
    this.props.store.saveTask(name, status);
  }

  render() {
    var { task, index } = this.props;
    return (
      <tr>
        <td> {index + 1} </td>
        <td>{task.name}</td>
        <td className="text-center">
          <button className={task.status ? 'alert alert-danger' : 'alert alert-success'}
            onClick={() => task.updateStatus()}
          >
            {task.status ? 'Kích hoạt' : 'Ẩn'}
          </button>
        </td>
        <td className="text-center">
          <button
            type="button"
            className="btn btn-warning"
            onClick={() => { this.dataEdit(task) }
            }
          >
            <span className="fas fa-edit mr-2"
            />Sửa
            </button>
          <button
            type="button"
            className="btn btn-danger"
            onClick={() => task.remove()}
          >
            <span className="fas fa-trash mr-2" />Xóa
            </button>
        </td>
      </tr>
    );
  };
}

export default observer(TaskItem);
