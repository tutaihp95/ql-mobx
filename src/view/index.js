import React, { Component } from 'react';
// import './../App.css';
import Header from './components/Header/index';
import Body from './components/Main/index';
class View extends Component {

  constructor(props) {
    super(props)
    this.myRef = React.createRef()
    this.state = { scrollTop: 0 }
  }

  onScroll = () => {
    const scrollTop = this.myRef.current.scrollTop;
    const page = document.getElementById("page");
    this.setState({
      scrollTop: scrollTop
    });
    if (scrollTop > 340) {
      page.classList.add("scrollTop");
    } else {
      page.classList.remove("scrollTop");
    }
  }

  render() {
    return (
      <div ref={this.myRef}
        onScroll={this.onScroll}
        style={{
          height: '100vh',
          overflow: 'scroll',
        }}
      >

        <section id="page">
          <div className="header-wrapper avatar-style-circle">
            <Header />
          </div>
          <Body />
          {/* <footer id="footer" className="content clearfix">
                            <div id="pagination">
                            <a href="/page/2" className="next invisible" data-current-page={1} data-total-pages={2513}>Next<span className="bg" /></a>
                            <div className="loader"><div className="loader-bar" /><div className="loader-bar" /><div className="loader-bar" /></div>
                            </div>
                        </footer> */}
        </section>
      </div>

    );
  };
}

export default View;
