import React, {Component} from 'react';

class BackgroundInfo extends Component{
    constructor(props){
        super(props);
        this.state= {
            bgr : '/img/background.jpg',
            avatar : '/img/avatar.jpeg',
            blog_title : 'A Girl Magazine',
        }
    }
    render() {
        var {bgr, avatar, blog_title } = this.state;
        return (
            <div>
                <header id="header" className="top-blog-header">
                    <div className="header-image-wrapper ">
                        <a href="/" className="header-image parallax cover loaded" style={{ backgroundImage: "url("+bgr+")" }}>
                        </a>

                    </div>
                    <div className="blog-title-wrapper content">
                        <figure className="avatar-wrapper animate">
                        <a href="/" className="user-avatar" style={{ backgroundImage: "url("+avatar+")" }}>
                            <img alt="avatar" className="print-only invisible" /></a>
                        </figure>
                        <div className="title-group animate">
                        <h1 className="blog-title"><a href="/">{blog_title}</a></h1>
                        <span className="description" style={{color: 'rgba(34, 75, 55, 0.7)'}}>
                            Watching beautiful girls pictures will increase your life expectancy up to 10 years. Therefore <br />
                            A Girl Magazine (https://agirlmagazine.tumblr.com) was born \m/<br />
                            Keywords:<br />
                            Vietnamese hot girls, girls, model, teen girls..
                        </span>
                        </div>
                    </div>
                </header>
            </div> 
            
        );
    };
}

export default BackgroundInfo;
