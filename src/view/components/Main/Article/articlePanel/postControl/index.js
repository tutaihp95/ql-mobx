import React, {Component} from 'react';
import ButtonNote from './buttonNote';
import ButtonControl from './buttonControl';

class PostControl extends Component{

    render() {
        return (
                   
            <section className="panel">

                <ButtonNote articlesPanel={this.props.articlesPanel} onIsDisplayNote={this.props.onIsDisplayNote} />

                <ButtonControl/>

            </section>
        );
    };
}

export default PostControl;
