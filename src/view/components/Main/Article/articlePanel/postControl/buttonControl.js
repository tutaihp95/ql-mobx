import React, {Component} from 'react';

class ButtonControl extends Component{
    
    render() {
        return (
            <section className="post-controls">
                <div className="controls-wrapper">
                    <div className="control share-control">
                        <nav className="pop">
                            <div className="btn-group dropup">
                                <a className="share selector" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="/" >
                                </a>

                                <div className="dropdown-menu dropdown-menu-right">
                                <ul>
                                    <li className="open-in-app"><a href="/" className="share-item open-in-app" >Open in app</a></li>
                                    <li><a href="/" className="share-item facebook" target="_blank">Facebook</a></li>
                                    <li><a href="/" className="share-item twitter" target="_blank">Tweet</a></li>
                                    <li><a href="/" className="share-item pinterest" target="_blank">Pinterest</a></li>
                                    <li><a href="/" className="share-item reddit" target="_blank">Reddit</a></li>
                                    <li><a href="/" className="share-item mail">Mail</a></li>
                                    <li><a href="/" className="share-item permalink">Embed</a></li>
                                    <li><a href="/" className="share-item permalink">Permalink <i className="arrow" /></a></li>
                                </ul>
                                </div>
                            </div>
                        </nav>
                        
                    </div>
                    <div className="control reblog-control">
                        <a href="/" className="reblog_button" style={{display: 'block', width: 24, height: 24}}>
                            <svg width="100%" height="100%" viewBox="0 0 21 21" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" fill="#000">
                                <path d="M5.01092527,5.99908429 L16.0088498,5.99908429 L16.136,9.508 L20.836,4.752 L16.136,0.083 L16.1360004,3.01110845 L2.09985349,3.01110845 C1.50585349,3.01110845 0.979248041,3.44726568 0.979248041,4.45007306 L0.979248041,10.9999998 L3.98376463,8.30993634 L3.98376463,6.89801007 C3.98376463,6.20867902 4.71892527,5.99908429 5.01092527,5.99908429 Z" />
                                <path d="M17.1420002,13.2800293 C17.1420002,13.5720293 17.022957,14.0490723 16.730957,14.0490723 L4.92919922,14.0490723 L4.92919922,11 L0.5,15.806 L4.92919922,20.5103758 L5.00469971,16.9990234 L18.9700928,16.9990234 C19.5640928,16.9990234 19.9453125,16.4010001 19.9453125,15.8060001 L19.9453125,9.5324707 L17.142,12.203" />
                            </svg>
                        </a>
                    </div>
                    <div className="control like-control">
                        <div className="like_button" data-blog-name="agirlmagazine">
                            {/* <iframe src="/" scrolling="no" width={24} height={24} frameBorder={0} className="like_toggle" allowTransparency="true" /> */}
                        </div>
                    </div>
                </div>
            </section>
        );
    };
}

export default ButtonControl;
