import React, {Component} from 'react';
import NoteDate from './NoteDate/index';
import PostControl from './postControl/index';

class ArticlePanel extends Component{

    constructor(props){
        super(props);
        this.state={
            isDisplayNote: false,
        };
    }

    onIsDisplayNote=()=>{
        this.setState({
            isDisplayNote: !this.state.isDisplayNote,
        });
    }
    render() {
        var {articlesPanel} = this.props.article;
        return (
            <section className="panel">
                <footer className="post-footer">

                    <PostControl articlesPanel={articlesPanel} onIsDisplayNote={this.onIsDisplayNote}/>

                    <NoteDate articlesPanel={articlesPanel} onIsDisplayNoteDate={this.state.isDisplayNote}/>
                </footer>
            </section>

        );
    };
}

export default ArticlePanel;
