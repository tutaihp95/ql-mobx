import React, {Component} from 'react';
import PostContent from './postContent';

class ArticlePost extends Component{

    render() {
        var {articlesPost} = this.props.article;
        
        return (
          
            <section className="post">
                
                <PostContent articlesPost={articlesPost}/>

                <section className="inline-meta post-extra has-tags">
                    <a className="meta-item tag-link" href="/">sose.xyz</a>
                    <a className="meta-item tag-link" href="/">agirlmagazine</a>
                    <a className="meta-item tag-link" href="/">vienamese girls</a>
                    <a className="meta-item tag-link" href="/">vietnamese</a>
                </section>
            </section>

        );
    };
}

export default ArticlePost;
