import React, {Component} from 'react';

class PostContent extends Component{

    render() {
        var {articlesPost} = this.props;
        return (
            <figure className="post-content high-res with-caption">
                <div className="photo-wrapper">
                <div className="photo-wrapper-inner">
                    <a href="/">
                        <img src={ articlesPost.photo_wrapper } alt={articlesPost.caption} width={1280} height={852} />
                    </a>
                </div>
                </div>
                <figcaption className="caption">
                    <p>{articlesPost.caption}</p>
                </figcaption>
            </figure>
        );
    };
}

export default PostContent;
