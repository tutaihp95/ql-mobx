import React, {Component} from 'react';
import Article from './Article/index';
class Body extends Component{

    constructor(props){
        super(props);
        this.state={
            articles:[
                {
                    id: this.generateID(),
                    articlesPost:
                        {
                            photo_wrapper:'/img/tumblr.jpg',
                            caption:'',
                        },
                    articlesPanel:{
                        listNote:[
                            {
                                idNote:'',
                                avaNote:'https://assets.tumblr.com/images/default_avatar/cone_closed_64.png',
                                nameNote:'namminhanh',
                                Note:'liked this',
                            },
                            {
                                idNote:'',
                                avaNote:'https://assets.tumblr.com/images/default_avatar/pyramid_closed_64.png',
                                nameNote:'keensludgecolorkid',
                                Note:'liked this',
                            },
                        ],
                    },
                },
                {
                    id: this.generateID(),
                    articlesPost:
                        {
                            photo_wrapper:'/img/tumblr2.jpg',
                            caption:'',
                        },
                    articlesPanel:{
                        listNote:[
                            {
                                idNote:'',
                                avaNote:'https://assets.tumblr.com/images/default_avatar/cone_closed_64.png',
                                nameNote:'namminhanh',
                                Note:'liked this',
                            },
                            
                        ],
                    },
                },
                {
                    id: this.generateID(),
                    articlesPost:
                        {
                            photo_wrapper:'/img/tumblr3.jpg',
                            caption:'',
                        },
                    articlesPanel:{
                        listNote:[
                            {
                                idNote:'',
                                avaNote:'https://assets.tumblr.com/images/default_avatar/cone_closed_64.png',
                                nameNote:'namminhanh',
                                Note:'liked this',
                            },
                            
                        ],
                    },
                }
            ]
        };
    }

    s4(){
        return Math.floor((1+Math.random()) * 0x10000).toString(16).substring(1);
    }

    generateID(){
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4();
    }

    render() {
        var {articles} = this.state;
        var elmArticle = articles.map((article, index) =>{
            return <Article 
                        key={index}
                        index = {index}
                        article={article}
                    />
        })
        return (
            <section id="posts" className="content clearfix  avatar-style-circle  ">
                <div className="container">
                    <div className="main">
                        {elmArticle}
                    </div> 
                    <div className="sidebar">
                </div> 
                </div> 
            </section>
        );
    };
}

export default Body;
