import React, { Component } from 'react';
import './App.css';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import ViewAdmin from './viewAdmin/index';
import View from './view/index';
import { observer } from "mobx-react"

class App extends Component {

  render() {
    var { store } = this.props
    return (
      <Router store={store}>
        <Route exact path="/" component={View} />
        <Route path="/ViewAdmin" render={(props) => <ViewAdmin store={store} {...props} />} />
      </Router>
    );
  };
}

export default (App);
